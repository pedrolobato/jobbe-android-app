package br.com.teamcode.jobbe.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import br.com.teamcode.jobbe.R
import br.com.teamcode.jobbe.model.Professional
import br.com.teamcode.jobbe.model.Service
import br.com.teamcode.jobbe.util.Mask
import br.com.teamcode.jobbe.util.Render
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.UserProfileChangeRequest
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_contact_data.*
import kotlinx.android.synthetic.main.toolbar.*
import kotlinx.android.synthetic.main.toolbar.view.*
import java.util.*

class ContactDataActivity : AppCompatActivity() {

    private val db = FirebaseFirestore.getInstance()
    private val user = FirebaseAuth.getInstance().currentUser

    private var professional = Professional()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact_data)

        professional = intent.extras.get("professional") as Professional

        toolbar()
        inputs()

        done.isEnabled = false
        done.setOnClickListener { send() }

        done.isEnabled = formIsValid()
    }

    private fun formIsValid(): Boolean {
        var isValid = true

        when {
            input_cellphone.text.isEmpty() -> isValid = false
            input_cellphone.text.length < 15 -> isValid = false
        }

        return isValid

    }

    private fun toolbar() {
        toolbar.title.visibility = View.VISIBLE
        toolbar.line_bottom.visibility = View.VISIBLE
        toolbar.back.visibility = View.VISIBLE

        toolbar.title.text = getString(R.string.contact_data)
        back.setOnClickListener {
            onBackPressed()
        }
    }

    private fun inputs() {
        input_phone.addTextChangedListener(Mask.insert(input_phone, "(##) ####-####"))
        input_cellphone.addTextChangedListener(Mask.insert(input_cellphone, "(##) #####-####"))
        input_whatsapp.addTextChangedListener(Mask.insert(input_whatsapp, "(##) #####-####"))

        input_cellphone.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                done.isEnabled = formIsValid()
            }

        })

        if (professional.phone != null) input_phone.setText(professional.phone)
        if (professional.cellPhone != null) input_cellphone.setText(professional.cellPhone)
        if (professional.whatsApp != null) input_whatsapp.setText(professional.whatsApp)
    }

    private fun send() {
        if (user != null) {
            Render.loading(root_contact_data, true)

            professional.id = user.uid
            professional.phone = input_phone.text.toString()
            professional.cellPhone = input_cellphone.text.toString()
            professional.whatsApp = input_whatsapp.text.toString()


            val professionalRef = db.collection("professional").document(user.uid)
            val services = intent.getSerializableExtra("services") as ArrayList<Service>
            val editMode = intent.getBooleanExtra("editMode", false)

            if (editMode) {
                professional.updateAt = Calendar.getInstance().time
            } else {
                professional.createAt = Calendar.getInstance().time
            }

            professionalRef
                    .set(professional)
                    .addOnSuccessListener {
                        services.forEach {
                            professionalRef
                                    .collection("services")
                                    .document(it.id!!)
                                    .set(it)
                                    .addOnSuccessListener { _ ->
                                        services.remove(it)
                                        if (services.isEmpty()) {
                                            db.collection("user")
                                                    .document(user.uid)
                                                    .update("professional", true)
                                                    .addOnSuccessListener {
                                                        if (editMode) {
                                                            finishAffinity()
                                                            startActivity(Intent(this, MainActivity::class.java)
                                                                    .putExtra("select_tab", "menu"))
                                                            startActivity(Intent(this, ProfileProfessionalActivity::class.java)
                                                                    .putExtra("id", user.uid))
                                                        } else {
                                                            finishAffinity()
                                                            user.updateProfile(
                                                                    UserProfileChangeRequest.Builder()
                                                                            .setDisplayName(professional.name).build()
                                                            )
                                                            user.updateEmail(professional.email!!)
                                                            startActivity(Intent(this, MainActivity::class.java)
                                                                    .putExtra("select_tab", "menu"))
                                                        }
                                                    }
                                                    .addOnFailureListener {
                                                        Render.loading(root_contact_data, false)
                                                        Log.w("SEND_CONTACT_DATA", it)
                                                    }
                                        }
                                    }
                                    .addOnFailureListener {
                                        Render.loading(root_contact_data, false)
                                        Log.w("SEND_CONTACT_DATA", it)
                                    }
                        }
                    }
                    .addOnFailureListener {
                        Render.loading(root_contact_data, false)
                        Log.w("SEND_CONTACT_DATA", it)
                    }
        }
    }
}
