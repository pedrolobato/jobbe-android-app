package br.com.teamcode.jobbe.model

import java.util.*


class User(
        var id: String? = null,
        var isProfessional: Boolean = false,
        var createAt: String? = null,
        var updateAt: String? = null,
        var connectedAt: Date? = null
)