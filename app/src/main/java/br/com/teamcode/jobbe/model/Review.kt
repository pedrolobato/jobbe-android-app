package br.com.teamcode.jobbe.model

import java.io.Serializable
import java.util.*

/**
 * Created by Pedro Lobato on 13/03/2018.
 */
class Review(
        var description: String? = null,
        var number: Float = 0F,
        var userName: String? = null,
        var userId: String? = null,
        var createAt: Date? = null,
        var updateAt: Date? = null
) : Serializable