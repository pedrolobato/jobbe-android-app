package br.com.teamcode.jobbe.activity

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import br.com.teamcode.jobbe.R
import br.com.teamcode.jobbe.adapter.ItemListReviewAdapter
import br.com.teamcode.jobbe.model.Notification
import br.com.teamcode.jobbe.model.Professional
import br.com.teamcode.jobbe.model.Review
import br.com.teamcode.jobbe.util.Render
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.activity_profile_professional.*
import kotlinx.android.synthetic.main.toolbar.*
import kotlinx.android.synthetic.main.toolbar.view.*
import java.util.*


class ProfileProfessionalActivity : AppCompatActivity() {

    private val db = FirebaseFirestore.getInstance()
    private val user = FirebaseAuth.getInstance().currentUser

    private var professional = Professional()

    private var reviewEdit = false
    private var reviewOld = 0F
    private var review = Review()

    private val calendar = Calendar.getInstance()
    private var readMore = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_professional)

        Render.loading(root_profile_professional, true)

        val professionalRef = db.collection("professional")
                .document(intent.getStringExtra("id"))

        professionalRef
                .get()
                .addOnSuccessListener {
                    professional = it.toObject(Professional::class.java)

                    info()
                    reviews()
                    buttonFavorite()
                }
                .addOnFailureListener {
                    Log.w("PROFILE_PROFESSIONAL", it)
                    Render.loading(root_profile_professional, false)
                }

        toolbar()

        send_review.setOnClickListener { sendReview() }
        view_all_review.setOnClickListener {
            startActivity(Intent(this, ReviewsActivity::class.java)
                    .putExtra("id", professional.id))
        }

        checkUserReview()
    }

    private fun addNotification(notification: Notification) {
        if (user?.uid != null) {
            val professionalRef = db.collection("professional")
                    .document(professional.id!!)

            professionalRef.collection("notification")
                    .document(notification.id.toString())
                    .set(notification)
        }
    }

    private fun checkUserReview() {
        if (user != null) {
            val professionalRef = db.collection("professional")
                    .document(intent.getStringExtra("id"))
            val professionalReviews = professionalRef.collection("reviews")

            professionalReviews
                    .document(user.uid)
                    .get()
                    .addOnSuccessListener {
                        if (it.exists()) {
                            review = it.toObject(Review::class.java)
                            reviewEdit = true

                            reviewOld = review.number
                            review_professional.rating = review.number
                            review_description.setText(review.description)
                            send_review.text = getString(R.string.label_edit)
                        } else {
                            send_review.text = getString(R.string.label_send)
                        }
                    }
        }
    }

    private fun sendReview() {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(review_description.windowToken, 0)

        if (user != null) {
            Render.loading(root_profile_professional, true)

            if (professional.id != null) {
                val professionalRef = db.collection("professional")
                        .document(professional.id!!)
                val professionalReviews = professionalRef.collection("reviews")

                if (reviewEdit) {
                    review.updateAt = Calendar.getInstance().time
                } else {
                    review.createAt = Calendar.getInstance().time
                }

                review.userId = user.uid
                review.userName = user.displayName
                review.number = review_professional.rating
                review.description = review_description.text.toString()

                professionalReviews
                        .document(user.uid)
                        .set(review)
                        .addOnSuccessListener {

                            professionalReviews.get()
                                    .addOnSuccessListener {
                                        if (!reviewEdit) {
                                            professional.reviewCount += 1
                                        }
                                        professionalRef.get()
                                                .addOnSuccessListener {
                                                    if (reviewEdit) {
                                                        professional.reviewAll -= reviewOld
                                                        professional.reviewAll += review.number
                                                    } else {
                                                        professional.reviewAll += review.number

                                                    }

                                                    professional.review = professional.reviewAll / professional.reviewCount

                                                    professionalRef.set(professional)
                                                            .addOnSuccessListener {
                                                                checkUserReview()
                                                                info()
                                                                reviews()
                                                                addNotification(Notification(calendar.timeInMillis,
                                                                        "${user.displayName} avaliou você.", calendar.time))
                                                            }
                                                            .addOnFailureListener {
                                                                Render.loading(root_profile_professional, false)
                                                            }
                                                }
                                                .addOnFailureListener {
                                                    Render.loading(root_profile_professional, false)
                                                }
                                    }
                                    .addOnFailureListener {
                                        Render.loading(root_profile_professional, false)
                                    }
                        }
                        .addOnFailureListener {
                            Render.loading(root_profile_professional, false)
                        }
            }

        }
    }

    private fun info() {
        setPhoto(professional.urlPhoto, photo)

        review_view.rating = professional.review
        review_number.text = String.format("%.1f", professional.review)
        review_view_general.rating = professional.review

        name.text = professional.name
        description.text = professional.description
        if (description.lineCount > 4) {
            description.maxLines = 4
            read_more.visibility = View.VISIBLE
        }
        read_more.setOnClickListener {
            if (readMore) {
                description.maxLines = description.lineCount
                read_more.text = getString(R.string.read_less)
                readMore = false
            } else {
                description.maxLines = 4
                read_more.text = getString(R.string.read_more)
                readMore = true
            }
        }
        services.text = professional.services

        phone1.text = professional.phone
        if (phone1.text.isEmpty()) {
            phone1.visibility = View.GONE
            ic_phone1.visibility = View.GONE
        }
        phone1.setOnClickListener {
            startActivity(Intent(Intent.ACTION_DIAL).setData(Uri.parse("tel:" + phone1.text)))
        }


        phone2.text = professional.cellPhone
        if (phone2.text.isEmpty()) {
            phone2.visibility = View.GONE
            ic_phone2.visibility = View.GONE
        }
        phone2.setOnClickListener {
            startActivity(Intent(Intent.ACTION_DIAL).setData(Uri.parse("tel:" + phone2.text)))
        }


        whatsapp.text = professional.whatsApp
        if (whatsapp.text.isEmpty()) {
            whatsapp.visibility = View.GONE
            ic_whatsapp.visibility = View.GONE
        } else {
            whatsapp.setOnClickListener {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://api.whatsapp.com/send?phone=+55" +
                        whatsapp.text.toString().replace("[\\D]+".toRegex(), ""))))
            }
        }

        if (professional.id == user?.uid) {
            line_top_review.visibility = View.GONE
            wrap_review.visibility = View.GONE
            toolbar.favorite.visibility = View.GONE
        } else {
            setPhoto(user?.photoUrl.toString(), photo_user)
            line_top_review.visibility = View.VISIBLE
            wrap_review.visibility = View.VISIBLE
            toolbar.favorite.visibility = View.VISIBLE
        }
    }

    private fun setPhoto(url: String?, circleImageView: CircleImageView) {
        Picasso.get().load(url)
                .placeholder(R.drawable.ic_photo)
                .into(circleImageView)
    }

    private fun reviews() {
        val itemsReview = arrayListOf<Review>()

        professional.id?.let {
            db.collection("professional")
                    .document(it)
                    .collection("reviews")
                    .orderBy("createAt", Query.Direction.DESCENDING)
                    .limit(4)
                    .get()
                    .addOnSuccessListener {
                        if (it.isEmpty) {
                            Render.notFound(root_profile_professional, true,
                                    getString(R.string.not_reviews), R.drawable.ic_star_border)

                            view_all_review.visibility = View.GONE
                        } else {
                            Render.notFound(root_profile_professional, false)

                            if (it.size() < 4) view_all_review.visibility = View.GONE

                            it.forEach {
                                itemsReview.add(it.toObject(Review::class.java))
                            }
                            items_review.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
                            items_review.adapter = ItemListReviewAdapter(itemsReview)
                            items_review.isNestedScrollingEnabled = false
                        }

                        Render.loading(root_profile_professional, false)
                    }
                    .addOnFailureListener {
                        Log.w("GET_REVIEWS", it)
                        Render.loading(root_profile_professional, false)
                    }
        }
    }

    private fun toolbar() {
        toolbar.title.visibility = View.VISIBLE
        toolbar.line_bottom.visibility = View.VISIBLE
        toolbar.back.visibility = View.VISIBLE

        toolbar.title.text = getString(R.string.profile)
        toolbar.back.setOnClickListener { finish() }
        toolbar.favorite.setOnClickListener { toggleFavorite() }
    }

    private fun buttonFavorite() {
        if (user != null) {
            db.collection("user")
                    .document(user.uid)
                    .collection("favorites")
                    .document(professional.id!!)
                    .get()
                    .addOnCompleteListener {
                        if (it.result.exists()) {
                            toolbar.favorite.setImageResource(R.drawable.ic_favorite_selected)
                            toolbar.favorite.visibility = View.VISIBLE
                        }
                    }
        }
    }

    private fun toggleFavorite() {
        if (user != null) {
            db.collection("user")
                    .document(user.uid)
                    .collection("favorites")
                    .document(professional.id!!)
                    .get()
                    .addOnCompleteListener {
                        if (!it.result.exists()) {
                            db.collection("user")
                                    .document(user.uid)
                                    .collection("favorites")
                                    .document(professional.id!!)
                                    .set(professional)
                                    .addOnSuccessListener {
                                        toolbar.favorite.setImageResource(R.drawable.ic_favorite_selected)
                                        Snackbar.make(root_profile_professional, getString(R.string.add_favorites),
                                                Snackbar.LENGTH_SHORT).show()
                                    }
                                    .addOnFailureListener {
                                        Log.w("ADD_FAVORITE", it)
                                    }
                        } else {
                            db.collection("user")
                                    .document(user.uid)
                                    .collection("favorites")
                                    .document(professional.id!!)
                                    .delete()
                                    .addOnSuccessListener {
                                        toolbar.favorite.setImageResource(R.drawable.ic_favorite_unselected)
                                        Snackbar.make(root_profile_professional, getString(R.string.remove_favorites),
                                                Snackbar.LENGTH_SHORT).show()
                                    }
                                    .addOnFailureListener {
                                        Log.w("REMOVE_FAVORITE", it)
                                    }
                        }
                    }
        }
    }
}
