package br.com.teamcode.jobbe.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.teamcode.jobbe.R
import br.com.teamcode.jobbe.model.Notification
import br.com.teamcode.jobbe.util.Time
import kotlinx.android.synthetic.main.item_list_notification.view.*


class ItemListNotificationAdapter(private val itemsNotification: List<Notification>, private val listener: (Notification) -> Unit)
    : RecyclerView.Adapter<ItemListNotificationAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.item_list_notification, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = itemsNotification.size

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        val itemNotification = itemsNotification[position]
        holder?.bind(itemNotification, listener)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(notification: Notification, listener: (Notification) -> Unit) {
            itemView.message.text = notification.message
            itemView.time.text = Time.getHoursAndMinutes(notification.time)
            itemView.setOnClickListener { listener(notification) }
        }
    }
}