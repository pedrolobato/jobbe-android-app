package br.com.teamcode.jobbe.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.teamcode.jobbe.R
import br.com.teamcode.jobbe.adapter.ItemListProfessionalAdapter
import br.com.teamcode.jobbe.adapter.ItemListServiceAdapter
import br.com.teamcode.jobbe.model.Professional
import br.com.teamcode.jobbe.model.Service
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.fragment_service.view.*
import kotlinx.android.synthetic.main.loading.view.*
import kotlinx.android.synthetic.main.toolbar.view.*


class ServiceFragment : Fragment() {

    private val db = FirebaseFirestore.getInstance()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_service, container, false)

        renderLoading(view, true)
        toolbar(view)
        itemsService(view)

        return view
    }

    private fun toolbar(view: View) {
        view.toolbar.title.visibility = View.VISIBLE
        view.toolbar.back.visibility = View.VISIBLE
        view.toolbar.line_bottom.visibility = View.VISIBLE

        view.toolbar.title.text = this.arguments?.get("description").toString()
        view.toolbar.back.setOnClickListener { activity?.onBackPressed() }
    }

    private fun itemsService(view: View) {
        val services = mutableListOf<Service>()

        db.collection("category")
                .document(this.arguments?.get("id").toString())
                .collection("services")
                .orderBy("description")
                .get()
                .addOnCompleteListener {
                    if (it.isSuccessful) {
                        for (document in it.result) {
                            services.add(document.toObject(Service::class.java))
                        }
                        view.items_service.layoutManager = LinearLayoutManager(view.context, LinearLayoutManager.VERTICAL, false)
                        view.items_service.adapter = ItemListServiceAdapter(services, { service ->
                            itemServiceClicked(service)
                        })
                        renderLoading(view, false)
                    } else {
                        Log.w("SERVICES", it.exception)
                    }
                }
    }

    private fun itemServiceClicked(service: Service) {
        val professionalsFragment = ProfessionalsFragment()
        val bundle = Bundle()

        bundle.putSerializable("service", service)

        professionalsFragment.arguments = bundle

        activity?.supportFragmentManager
                ?.beginTransaction()
                ?.replace(R.id.content, professionalsFragment)
                ?.addToBackStack("professionals_fragment")
                ?.commit()
    }

    private fun renderLoading(view: View, render: Boolean) {
        view.loading.visibility = if (render) View.VISIBLE else View.GONE
    }

}
