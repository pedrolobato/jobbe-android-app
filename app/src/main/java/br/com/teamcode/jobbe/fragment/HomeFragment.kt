package br.com.teamcode.jobbe.fragment

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.teamcode.jobbe.R
import br.com.teamcode.jobbe.activity.ProfileProfessionalActivity
import br.com.teamcode.jobbe.adapter.ItemListCategoryAdapter
import br.com.teamcode.jobbe.adapter.ItemListProfessionalAdapter
import br.com.teamcode.jobbe.adapter.ItemListServiceSuggestionsAdapter
import br.com.teamcode.jobbe.model.Category
import br.com.teamcode.jobbe.model.Professional
import br.com.teamcode.jobbe.model.Service
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.view.*
import kotlinx.android.synthetic.main.loading.view.*
import kotlinx.android.synthetic.main.not_found.view.*
import kotlinx.android.synthetic.main.toolbar.view.*


class HomeFragment : Fragment() {

    private val db = FirebaseFirestore.getInstance()

    private var itemListCategoryAdapter: ItemListCategoryAdapter? = null
    private var itemListServiceSuggestionsAdapter: ItemListServiceSuggestionsAdapter? = null
    private var itemListProfessionalAdapter: ItemListProfessionalAdapter? = null

    private val professionals = arrayListOf<Professional>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_home, container, false)

        toolbar(view)
        search(view)
        itemsCategory(view)
        itemsServiceSuggestions(view)
        itemsProfessional(view)

        return view
    }

    private fun toolbar(view: View) {
        view.toolbar.logo.visibility = View.VISIBLE
        view.toolbar.line_bottom.visibility = View.VISIBLE
    }

    private fun search(view: View) {
        view.search.setOnQueryTextFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                itemsServiceSuggestions(view)

                renderItemsCategory(view, false)
                renderItemsProfessional(view, false)
                renderNotFound(view, false)
            } else if (view.items_suggestion_service.visibility == View.GONE &&
                    view.items_professional.visibility == View.GONE) {
                renderItemsCategory(view, true)
            }
        }

        view.search.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                renderLoading(view, true)

                renderItemsProfessional(view, true)
                renderItemsServiceSuggestions(view, false)
                renderNotFound(view, false)

                professionals.clear()

                if (query != null) {
                    db.collection("professional")
                            .get()
                            .addOnSuccessListener {
                                it.documents.forEach {
                                    val services = it.data["services"].toString().toLowerCase()

                                    if (services.contains(query.toLowerCase())) {
                                        professionals.add(it.toObject(Professional::class.java))
                                    }
                                }

                                renderLoading(view, false)

                                if (professionals.isEmpty()) {
                                    renderNotFound(
                                            view, true,
                                            getString(R.string.not_found_professional),
                                            R.drawable.ic_sentiment_dissatisfied)
                                } else {
                                    renderNotFound(view, false)
                                }

                                itemListProfessionalAdapter?.notifyDataSetChanged()
                            }
                            .addOnFailureListener {
                                Log.w("SEARCH", it)
                            }
                }
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                newText?.let {
                    if (newText.isEmpty()) {
                        renderItemsServiceSuggestions(view, false)
                        renderItemsProfessional(view, false)
                        renderNotFound(view, false)
                    } else {
                        if (newText.length > 1) {
                            itemListServiceSuggestionsAdapter?.filter(it)
                            itemListServiceSuggestionsAdapter?.notifyDataSetChanged()
                            renderItemsServiceSuggestions(view, true)
                            renderItemsProfessional(view, false)
                        }
                    }
                }
                return false
            }
        })
    }

    private fun itemsCategory(view: View) {
        val categories = this.arguments?.get("categories") as ArrayList<Category>

        itemListCategoryAdapter = ItemListCategoryAdapter(categories, { goServiceFragment(it) })

        view.items_category.layoutManager = LinearLayoutManager(view.context, LinearLayoutManager.VERTICAL, false)
        view.items_category.adapter = itemListCategoryAdapter
        view.items_category.isNestedScrollingEnabled = false
    }

    private fun goServiceFragment(category: Category) {
        val serviceFragment = ServiceFragment()
        val bundle = Bundle()

        bundle.putString("id", category.id)
        bundle.putString("description", category.description)

        serviceFragment.arguments = bundle

        activity?.supportFragmentManager
                ?.beginTransaction()
                ?.replace(R.id.content, serviceFragment)
                ?.addToBackStack("service_fragment")
                ?.commit()
    }

    private fun itemsServiceSuggestions(view: View) {
        val suggestions = this.arguments?.get("suggestions") as ArrayList<Service>

        itemListServiceSuggestionsAdapter = ItemListServiceSuggestionsAdapter(suggestions, {
            itemSuggestionClicked(it)
        })

        view.items_suggestion_service.layoutManager = LinearLayoutManager(view.context, LinearLayoutManager.VERTICAL, false)
        view.items_suggestion_service.adapter = itemListServiceSuggestionsAdapter
        view.items_suggestion_service.isNestedScrollingEnabled = false
    }

    private fun itemSuggestionClicked(service: Service) {
        search.setQuery(service.description, true)
    }

    private fun itemsProfessional(view: View) {
        itemListProfessionalAdapter = ItemListProfessionalAdapter(professionals, {
            itemProfessionalClicked(it)
        })

        view.items_professional.layoutManager = LinearLayoutManager(view.context, LinearLayoutManager.VERTICAL, false)
        view.items_professional.adapter = itemListProfessionalAdapter
        view.items_professional.isNestedScrollingEnabled = false
    }

    private fun itemProfessionalClicked(professional: Professional) {
        startActivity(Intent(this.context, ProfileProfessionalActivity::class.java)
                .putExtra("id", professional.id))
    }

    private fun renderItemsCategory(view: View, render: Boolean) {
        view.wrap_items_category.visibility = if (render) View.VISIBLE else View.GONE
    }

    private fun renderItemsServiceSuggestions(view: View, render: Boolean) {
        view.items_suggestion_service.visibility = if (render) View.VISIBLE else View.GONE
    }

    private fun renderItemsProfessional(view: View, render: Boolean) {
        view.items_professional.visibility = if (render) View.VISIBLE else View.GONE
    }

    private fun renderLoading(view: View, render: Boolean) {
        view.loading.visibility = if (render) View.VISIBLE else View.GONE
    }

    private fun renderNotFound(view: View, render: Boolean, message: String? = null, icon: Int? = null) {
        view.not_found.visibility = if (render) View.VISIBLE else View.GONE
        view.not_found.message.text = message
        if (icon != null) {
            view.not_found.icon.setImageResource(icon)
        }
    }

}
