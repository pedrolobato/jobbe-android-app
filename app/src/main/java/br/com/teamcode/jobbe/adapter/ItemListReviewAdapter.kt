package br.com.teamcode.jobbe.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.teamcode.jobbe.R
import br.com.teamcode.jobbe.model.Review
import kotlinx.android.synthetic.main.item_list_review.view.*


class ItemListReviewAdapter(private val itemsReview: ArrayList<Review>)
    : RecyclerView.Adapter<ItemListReviewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.item_list_review, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = itemsReview.size

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        val itemReview = itemsReview[position]
        holder?.bind(itemReview)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(review: Review) {
            itemView.number.text = String.format("%.1f", review.number)
            itemView.review_professional.rating = review.number
            itemView.user_name.text = review.userName
            itemView.description.text = review.description
        }
    }

}