package br.com.teamcode.jobbe.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import br.com.teamcode.jobbe.R
import br.com.teamcode.jobbe.model.User
import br.com.teamcode.jobbe.util.Render
import com.facebook.*
import com.facebook.appevents.AppEventsLogger
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity : AppCompatActivity() {

    private val callbackManager = CallbackManager.Factory.create()
    private val tag = "signInFacebook"
    private val auth = FirebaseAuth.getInstance()
    private val db = FirebaseFirestore.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        FacebookSdk.sdkInitialize(applicationContext)
        AppEventsLogger.activateApp(this)

        LoginManager.getInstance().registerCallback(callbackManager, object :
                FacebookCallback<LoginResult> {
            override fun onSuccess(result: LoginResult?) {
                Log.d(tag, "facebook:onSuccess:$result")
                result?.let { handleFacebookAccessToken(it.accessToken) }
            }

            override fun onCancel() {
                Log.d(tag, "facebook:onCancel")
            }

            override fun onError(error: FacebookException?) {}

        })

        if (auth.currentUser != null) {
            startActivity(Intent(this, MainActivity::class.java))
        }

        initUI()
    }

    override fun onRestart() {
        super.onRestart()
        Render.loading(login, false)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callbackManager.onActivityResult(requestCode, resultCode, data)
    }

    private fun initUI(){
        login_facebook.setOnClickListener { loginFacebook() }
        button_register.setOnClickListener { goSignUp() }
        button_login.setOnClickListener { goLogin() }
    }

    private fun handleFacebookAccessToken(token: AccessToken) {
        Log.d(tag, "handleFacebookAccessToken:$token")
        val credential = FacebookAuthProvider.getCredential(token.token)

        Render.loading(login, true)

        auth.signInWithCredential(credential)
                .addOnCompleteListener(this) {
                    if (it.isSuccessful) {
                        Log.d(tag, "signInWithCredential:success")
                        val uid = it.result.user.uid

                        db.collection("user")
                                .document(uid)
                                .get()
                                .addOnCompleteListener {
                                    if (!it.result.exists()) {
                                        db.collection("user")
                                                .document(uid)
                                                .set(User(uid))
                                                .addOnSuccessListener {
                                                    startActivity(Intent(this, MainActivity::class.java))
                                                }
                                    } else {
                                        startActivity(Intent(this, MainActivity::class.java))
                                    }
                                }
                    } else {
                        Render.loading(login, false)
                        Log.w(tag, "signInWithCredential:failure", it.exception)
                    }
                }
    }

    private fun loginFacebook() {
        LoginManager.getInstance().logInWithReadPermissions(this,
                arrayListOf("email", "public_profile"))
    }

    private fun goLogin() = startActivity(Intent(this, SignInActivity::class.java))

    private fun goSignUp() = startActivity(Intent(this, SignUpActivity::class.java))
}
