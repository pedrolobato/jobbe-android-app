package br.com.teamcode.jobbe.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import br.com.teamcode.jobbe.R
import br.com.teamcode.jobbe.util.Render
import kotlinx.android.synthetic.main.activity_reset_password.*
import kotlinx.android.synthetic.main.toolbar.*
import kotlinx.android.synthetic.main.toolbar.view.*
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.FirebaseAuthInvalidUserException


class ResetPasswordActivity : AppCompatActivity() {

    private val auth = FirebaseAuth.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reset_password)

        toolbar()
        send_email.setOnClickListener { send() }
        input_email.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                send_email.isEnabled = input_email.text.isNotEmpty()
            }

        })
    }

    private fun toolbar() {
        toolbar.title.visibility = View.VISIBLE
        toolbar.title.text = getText(R.string.label_reset_password)

        toolbar.back.visibility = View.VISIBLE
        back.setOnClickListener { finish() }
    }

    private fun send() {
        Render.loading(root_reset_password, true)
        val email = input_email.text.toString()
        auth?.sendPasswordResetEmail(email)
                ?.addOnSuccessListener {
                    Render.loading(root_reset_password, false)
                    val alert = AlertDialog.Builder(this, R.style.DialogTheme)
                    alert.setTitle(getString(R.string.send_success))
                    alert.setMessage("Um e-mail foi enviado para $email")
                    alert.setPositiveButton(getText(R.string.ok), { dialog, _ ->
                        dialog.dismiss()
                        finish()
                    })
                    alert.show()
                }?.addOnFailureListener { exception ->
                    Render.loading(root_reset_password, false)
                    when (exception) {
                        is FirebaseAuthInvalidCredentialsException -> {
                            when (exception.errorCode) {
                                "ERROR_INVALID_EMAIL" -> {
                                    input_layout_email.error = getString(R.string.error_email_invalid)
                                    input_layout_email.requestFocus()
                                }
                                else -> Log.w("RESET_PASSWORD", exception.errorCode)
                            }
                        }
                        is FirebaseAuthInvalidUserException -> {
                            when (exception.errorCode) {
                                "ERROR_USER_NOT_FOUND" -> {
                                    input_layout_email.error = getString(R.string.error_email_not_found)
                                    input_layout_email.requestFocus()
                                }
                                else -> Log.w("RESET_PASSWORD", exception.errorCode)
                            }
                        }
                        else -> Log.w("RESET_PASSWORD", exception)
                    }
                }
    }
}
