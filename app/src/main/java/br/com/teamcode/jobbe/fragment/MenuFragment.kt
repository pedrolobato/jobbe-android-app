package br.com.teamcode.jobbe.fragment


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.teamcode.jobbe.R
import br.com.teamcode.jobbe.activity.*
import br.com.teamcode.jobbe.adapter.ItemListMenuAdapter
import br.com.teamcode.jobbe.model.Menu
import com.google.firebase.auth.FirebaseAuth
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_menu.view.*
import kotlinx.android.synthetic.main.toolbar.view.*

class MenuFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_menu, container, false)

        toolbar(view)
        menu(view)

        return view
    }

    private fun toolbar(view: View) {
        view.toolbar.logo.visibility = View.VISIBLE
        view.toolbar.line_bottom.visibility = View.VISIBLE
    }

    private fun menu(view: View) {
        val id = this.arguments?.getString("id")
        val photoUrl = this.arguments?.getString("photoUrl")
        val name = this.arguments?.getString("name")
        val isProfessional = this.arguments?.getBoolean("isProfessional")
        val email = this.arguments?.getString("email")

        photoUrl?.let { Picasso.get().load(it).placeholder(R.drawable.ic_photo).into(view.info_user.photo) }

        name?.let { view.info_user.name.text = name }

        isProfessional?.let {
            if (it) {
                view.info_user.subtitle.text = getString(R.string.view_profile)
                view.info_user.setOnClickListener {
                    startActivity(Intent(this.context, ProfileProfessionalActivity::class.java)
                            .putExtra("id", id))
                }
            } else {
                view.info_user.subtitle.text = email?.let { it }
            }

            view.items_menu.layoutManager = LinearLayoutManager(view.context, LinearLayoutManager.VERTICAL, false)
            view.items_menu.adapter = ItemListMenuAdapter(itemsMenu(it), { itemMenuClicked(it) })
        }
    }

    private fun itemMenuClicked(menu: Menu) {
        when (menu.id) {
            "register_professional" -> {
                startActivity(Intent(this.context, PersonalDataActivity::class.java))
            }
            "favorites" -> {
                startActivity(Intent(this.context, FavoritesActivity::class.java))
            }
            "edit_profile" -> {
                startActivity(Intent(this.context, ProfileDataActivity::class.java)
                        .putExtra("editMode", true))
            }
            "edit_personal_data" -> {
                startActivity(Intent(this.context, PersonalDataActivity::class.java)
                        .putExtra("editMode", true))
            }
            "reviews" -> {
                startActivity(Intent(this.context, ReviewsActivity::class.java))
            }
            "logout" -> {
                val auth = FirebaseAuth.getInstance()
                auth.signOut()
                auth.addAuthStateListener {
                    if (it.currentUser == null) {
                        activity?.finish()
                    }
                }
            }
        }
    }

    private fun itemsMenu(isProfessional: Boolean): List<Menu> {
        return if (isProfessional) {
            listOf(
                    Menu("edit_profile", "Dados Perfil"),
                    Menu("edit_personal_data", "Dados Pessoais"),
                    Menu("favorites", "Favoritos"),
                    Menu("reviews", "Resenhas"),
                    Menu("logout", "Sair")
            )
        } else {
            listOf(
                    Menu("register_professional", "Cadastrar como Profissional"),
                    Menu("favorites", "Favoritos"),
                    Menu("logout", "Sair")
            )
        }

    }

}
