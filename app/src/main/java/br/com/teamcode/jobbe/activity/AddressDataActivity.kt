package br.com.teamcode.jobbe.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import br.com.teamcode.jobbe.R
import br.com.teamcode.jobbe.model.Professional
import br.com.teamcode.jobbe.util.Mask
import br.com.teamcode.jobbe.util.Render
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.UserProfileChangeRequest
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_address_data.*
import kotlinx.android.synthetic.main.toolbar.*
import kotlinx.android.synthetic.main.toolbar.view.*
import java.util.*

class AddressDataActivity : AppCompatActivity() {

    private val db = FirebaseFirestore.getInstance()
    private val user = FirebaseAuth.getInstance().currentUser

    private var professional = Professional()
    private var editMode = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_address_data)

        editMode = intent.getBooleanExtra("editMode", false)

        professional = intent.extras.get("professional") as Professional
        inputs()

        toolbar()

        done.isEnabled = false
        done.setOnClickListener { send() }

        done.isEnabled = formIsValid()
    }

    private fun toolbar() {
        toolbar.title.visibility = View.VISIBLE
        toolbar.line_bottom.visibility = View.VISIBLE
        toolbar.back.visibility = View.VISIBLE

        toolbar.title.text = getString(R.string.address_data)
        back.setOnClickListener {
            onBackPressed()
        }
    }

    private fun formIsValid(): Boolean {
        var isValid = true

        when {
            input_cep.text.isEmpty() -> isValid = false
            input_cep.text.length < 10 -> isValid = false
            input_address.text.isEmpty() -> isValid = false
            input_number.text.isEmpty() -> isValid = false
        }

        return isValid
    }

    private fun inputs() {
        input_cep.addTextChangedListener(Mask.insert(input_cep, "##.###-###"))

        input_cep.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                done.isEnabled = formIsValid()
            }

        })
        input_address.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                done.isEnabled = formIsValid()
            }

        })
        input_number.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                done.isEnabled = formIsValid()
            }

        })

        if (professional.cep != null) {
            input_cep.setText(professional.cep)
        }
        if (professional.address != null) {
            input_address.setText(professional.address)
        }
        if (professional.addressNumber != null) {
            input_number.setText(professional.addressNumber)
        }
    }

    private fun send() {
        professional.cep = input_cep.text.toString()
        professional.address = input_address.text.toString()
        professional.addressNumber = input_number.text.toString()

        if (editMode) {
            Render.loading(root_address_data, true)
            val professionalRef = db.collection("professional").document(professional.id!!)
            professionalRef.set(professional)
                    .addOnSuccessListener {
                        finishAffinity()
                        user?.updateProfile(
                                UserProfileChangeRequest.Builder()
                                        .setDisplayName(professional.name).build()
                        )
                        user?.updateEmail(professional.email!!)
                        startActivity(Intent(this, MainActivity::class.java)
                                .putExtra("select_tab", "menu"))
                        professionalRef.update("updateAt", Calendar.getInstance().time)
                    }
                    .addOnFailureListener {
                        Render.loading(root_address_data, false)
                        Log.w("SAVE_PROFESSIONAL", it)
                    }
        } else {
            startActivity(Intent(this, ProfileDataActivity::class.java)
                    .putExtra("professional", professional))
        }

    }
}
