package br.com.teamcode.jobbe.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import br.com.teamcode.jobbe.R
import br.com.teamcode.jobbe.model.Professional
import br.com.teamcode.jobbe.util.Mask
import br.com.teamcode.jobbe.util.Render
import br.com.teamcode.jobbe.util.Validation
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_personal_data.*
import kotlinx.android.synthetic.main.toolbar.*
import kotlinx.android.synthetic.main.toolbar.view.*

class PersonalDataActivity : AppCompatActivity() {

    private val db = FirebaseFirestore.getInstance()
    private val user = FirebaseAuth.getInstance().currentUser

    private var professional = Professional()

    private var editMode = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_personal_data)

        editMode = intent.getBooleanExtra("editMode", false)

        if (editMode) {
            if (user != null) {
                Render.loading(root_personal_data, true)

                val professionalRef = db.collection("professional").document(user.uid)

                professionalRef.get()
                        .addOnSuccessListener {
                            professional = it.toObject(Professional::class.java)
                            inputs()
                            Render.loading(root_personal_data, false)
                        }
                        .addOnFailureListener {
                            Render.loading(root_personal_data, false)
                            Log.w("GET_PROFESSIONAL", it)
                        }
            }

        } else {
            inputs()
        }

        toolbar()

        done.isEnabled = false
        done.setOnClickListener { send() }

        done.isEnabled = formIsValid()
    }

    private fun toolbar() {
        toolbar.title.visibility = View.VISIBLE
        toolbar.line_bottom.visibility = View.VISIBLE
        toolbar.back.visibility = View.VISIBLE

        toolbar.title.text = getString(R.string.personal_data)
        back.setOnClickListener {
            onBackPressed()
        }
    }

    private fun formIsValid(): Boolean {
        var isValid = true

        when {
            input_name.text.isEmpty() -> isValid = false
            input_cpf_cnpj.text.isEmpty() -> isValid = false
            input_cpf_cnpj.text.length < 14 -> isValid = false
            input_email.text.isEmpty() -> isValid = false
        }

        if (input_cpf_cnpj.text.length == 14) {
            if (!Validation.isValidCPF(input_cpf_cnpj.text.toString())) {
                isValid = false
            }
        }

        if (input_cpf_cnpj.text.length in 15..17) {
            isValid = false
        }

        if (input_cpf_cnpj.text.length == 18){
            if (!Validation.isValidCNPJ(input_cpf_cnpj.text.toString())) {
                isValid = false
            }
        }

        return isValid
    }

    private fun inputs() {
        if (user != null) {
            input_name.setText(user.displayName)
            input_email.setText(user.email)
        }
        input_cpf_cnpj.addTextChangedListener(Mask.insert(input_cpf_cnpj, "###.###.###-##",
                "##.###.###/####-##", 11))

        input_name.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                done.isEnabled = formIsValid()
            }

        })
        input_cpf_cnpj.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                done.isEnabled = formIsValid()
            }

        })
        input_email.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                done.isEnabled = formIsValid()
            }

        })

        if (professional.pfj != null) {
            input_cpf_cnpj.setText(professional.pfj)
        }

    }

    private fun send() {
        professional.name = input_name.text.toString()
        professional.pfj = input_cpf_cnpj.text.toString()
        professional.email = input_email.text.toString()

        if (editMode) {
            startActivity(Intent(this, AddressDataActivity::class.java)
                    .putExtra("professional", professional)
                    .putExtra("editMode", true))
        } else {
            startActivity(Intent(this, AddressDataActivity::class.java)
                    .putExtra("professional", professional))
        }
    }
}
