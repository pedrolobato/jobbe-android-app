package br.com.teamcode.jobbe.util

object Validation {

    private val weightCPF = intArrayOf(11, 10, 9, 8, 7, 6, 5, 4, 3, 2)
    private val weightCNPJ = intArrayOf(6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2)

    private fun computeDigit(str: String, weight: IntArray): Int {
        var sum = 0
        var index = str.length - 1
        var digit: Int
        while (index >= 0) {
            digit = Integer.parseInt(str.substring(index, index + 1))
            sum += digit * weight[weight.size - str.length + index]
            index--
        }
        sum = 11 - sum % 11
        return if (sum > 9) 0 else sum
    }

    fun isValidCPF(cpf: String?): Boolean {
        var cpf = cpf
        cpf = onlyNumbers((cpf as CharSequence?)!!)
        if (cpf.length != 11) return false

        val digitA = computeDigit(cpf.substring(0, 9), weightCPF)
        val digitB = computeDigit(cpf.substring(0, 9) + digitA, weightCPF)
        return cpf == cpf.substring(0, 9) + digitA.toString() + digitB.toString()
    }

    fun isValidCNPJ(cnpj: String?): Boolean {
        var cnpj = cnpj
        cnpj = onlyNumbers((cnpj as CharSequence?)!!)
        if (cnpj.length != 14) return false

        val digitA = computeDigit(cnpj.substring(0, 12), weightCNPJ)
        val digitB = computeDigit(cnpj.substring(0, 12) + digitA, weightCNPJ)
        return cnpj == cnpj.substring(0, 12) + digitA.toString() + digitB.toString()
    }

    private fun onlyNumbers(s: CharSequence): String {
        return s.toString().replace("\\D+".toRegex(), "")
    }

}