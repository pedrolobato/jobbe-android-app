package br.com.teamcode.jobbe.model

/**
 * Created by Pedro Lobato on 13/03/2018.
 */
class Menu(
        var id: String,
        var description: String
)