package br.com.teamcode.jobbe.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.teamcode.jobbe.R
import br.com.teamcode.jobbe.model.Menu
import kotlinx.android.synthetic.main.item_list_text.view.*


class ItemListMenuAdapter(private val itemsMenu: List<Menu>, private val listener: (Menu) -> Unit)
    : RecyclerView.Adapter<ItemListMenuAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.item_list_text, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = itemsMenu.size

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        val itemMenu = itemsMenu[position]
        holder?.bind(itemMenu, listener)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(menu: Menu, listener: (Menu) -> Unit) {
            itemView.divider_top.visibility = View.VISIBLE
            itemView.divider_bottom.visibility = View.GONE
            itemView.description.text = menu.description
            itemView.setOnClickListener { listener(menu) }
        }
    }

}