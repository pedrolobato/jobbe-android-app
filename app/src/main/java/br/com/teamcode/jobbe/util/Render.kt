package br.com.teamcode.jobbe.util

import android.view.View
import kotlinx.android.synthetic.main.loading.view.*
import kotlinx.android.synthetic.main.not_found.view.*

object Render {
    fun loading(view: View, render: Boolean, label: String? = null) {
        if (view.loading != null) {
            view.loading.visibility = if (render) View.VISIBLE else View.GONE
            view.loading.label.text = label
        }
    }

    fun notFound(view: View, render: Boolean, message: String? = null, icon: Int? = null) {
        if (view.not_found != null) {
            view.not_found.visibility = if (render) View.VISIBLE else View.GONE
            view.not_found.message.text = message
            if (icon != null) view.not_found.icon.setImageResource(icon)
        }
    }
}