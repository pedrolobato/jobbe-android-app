package br.com.teamcode.jobbe.util

import java.util.*

/**
 * Created by Pedro Lobato on 14/03/2018.
 */
object Time {

    fun getHoursAndMinutes(time: Date?): String {
        val date = Calendar.getInstance()
        date.time = time
        val hours = date.get(Calendar.HOUR_OF_DAY)
        val minutes = date.get(Calendar.MINUTE)

        return "%02d:%02d".format(hours, minutes)
    }
}