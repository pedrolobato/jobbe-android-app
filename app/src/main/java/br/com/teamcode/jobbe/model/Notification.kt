package br.com.teamcode.jobbe.model

import java.io.Serializable
import java.util.*


class Notification(
        var id: Long? = null,
        var message: String? = null,
        var time: Date? = null
) : Serializable