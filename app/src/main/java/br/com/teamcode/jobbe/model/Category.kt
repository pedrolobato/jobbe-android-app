package br.com.teamcode.jobbe.model

import java.io.Serializable

/**
 * Created by Pedro Lobato on 13/03/2018.
 */
class Category(
        var id: String? = null,
        var description: String? = null
) : Serializable